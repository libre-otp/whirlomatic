The Whirlomatic is a substitution cipher wheel that illustrates the stupidity of "golden key" crypto.  The intent is to target politicians using a parody of the 1930's-era Little Orphan Annie decoder wheel.

Golden Key
==========

The static key should correspond to the [letter frequency distribution](https://en.wikipedia.org/wiki/Letter_frequency) of the target language.  Since it is for demonstration purposes only, it should only use the lowercase alphabet (sans digits and special characters).

English
-------

| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 
|---|---|---|---|---|---|---|---|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----| 
| e | t | a | o | i | n | s | h | r | d  | l  | c  | u  | m  | w  | f  | g  | y  | p  | b  | v  | k  | j  | x  | q  | z  | 

Example Text
============

The canonical example should be article 19 of the [UN Declaration of Human Rights](http://www.un.org/en/universal-declaration-human-rights/index.html) ([translations](http://www.ohchr.org/EN/UDHR/Pages/SearchByLang.aspx)):

>  Everyone has the right to freedom of opinion and expression; this right includes freedom to hold opinions without interference and to seek, receive and impart information and ideas through any media and regardless of frontiers.

Of course, each local may have a better example, such as the United State's first amendment:

> Congress shall make no law respecting an establishment of religion, or prohibiting the free exercise thereof; or abridging the freedom of speech, or of the press; or the right of the people peaceably to assemble, and to petition the Government for a redress of grievances. 

Branding
========

Branding of Orphan Annie's Secret Society (SS) Whirlomatic decoder:

![image](https://gitlab.com/libre-otp/meta/uploads/780ddf6cce91ce28bc7168c3acf45e2a/image.png)
![image](https://gitlab.com/libre-otp/meta/uploads/f00d596a4be102c89db714b490f34528/image.png)
![image](https://gitlab.com/libre-otp/meta/uploads/96152f0aed08c92b2478065c92c74e77/image.png)
